//=============================================================================
//
// file :        EpicsThreadHandler.cpp
//
// description : Class for managing epics operations.
//
// project : Tango2Epics
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Author: Vid Juvan $
//
//=============================================================================

#include <EpicsThreadHandler.h>

bool EpicsThreadHandler::checkConn(thread_data* thread) {

  thread->state = ca_state(thread->channel);

  if(thread->state != cs_conn) {

    switch(thread->state) {
      case cs_never_conn:
        thread->error = thread->name + " valid chid, IOC not found";
        break;
      case cs_prev_conn:
        thread->error = thread->name + "valid chid, IOC was found, but unavailable";
        break;
      case cs_closed:
        thread->error = thread->name + "channel deleted by user";
        break;
    }

    thread->state = -1;
    if(thread->prevConnected) {
      //WARN_STREAM << "Epics channel connection lost for PV: " << thread->name << std::endl;
      thread->prevConnected = false;
    }
    return false;

  }

  //WARN_STREAM << "Epics channel successfully established for PV: " << thread->name << std::endl;
  thread->prevConnected = true;
  return true;

}

void EpicsThreadHandler::doRead(thread_data *thread) {

  if (!checkConn(thread))
    return;

  int status;
  unsigned long retSize = ca_element_count(thread->channel);
  if (retSize > thread->size) {
    thread->error = thread->name + ": Unexpected item count (max:" + to_string(thread->size) + " but got " +
                    to_string(retSize) + ")";
    thread->state = -1;
    return;
  }
  status = ca_array_get(thread->data_type, thread->size, thread->channel, thread->value);
  if (status != ECA_NORMAL) {
    thread->error = thread->name + " Error: " + ca_message(status);
    thread->state = -1;
    return;
  }
  status = ca_pend_io(thread->accessTimeout);
  if (status != ECA_NORMAL) {
    thread->error = thread->name + " Error: " + ca_message(status);
    thread->state = -1;
    return;
  }

}

void EpicsThreadHandler::doWrite(thread_data* thread) {

  if (!checkConn(thread))
    return;

  int status;
  status = ca_array_put(thread->data_type, thread->size, thread->channel, thread->value);
  if (status != ECA_NORMAL) {
    thread->error = thread->name + " Error: " + ca_message(status);
    thread->state = -1;
    return;
  }
  status = ca_pend_io(thread->accessTimeout);
  if (status != ECA_NORMAL) {
    thread->error = thread->name + " Error: " + ca_message(status);
    thread->state = -1;
  }

}


void* EpicsThreadHandler::subscription_thread_run(void* parm) {

	sub_thread_data* thread = (sub_thread_data*)parm;

	pthread_mutex_lock(&(thread->mutex));
	pthread_mutex_lock(&(thread->mutex));
	ca_context_create(ca_disable_preemptive_callback);
	ca_create_channel(thread->name.c_str(),NULL,NULL,10,&thread->channel);
	ca_create_subscription(thread->data_type,1,thread->channel,
			DBE_VALUE,thread->eventCallback, thread->callbackParam,&thread->event);
	pthread_mutex_unlock(&(thread->mutex));
	ca_pend_io(5.0);
	DEBUG_STREAM << "Epics subscription created for PV: " << thread->name << std::endl;

	bool alive = true;
	while(alive){
		pthread_mutex_lock(&(thread->mutex));
		alive = thread->keepAlive;
		pthread_mutex_unlock(&(thread->mutex));
		ca_pend_event(thread->subscriptionCycle);
	}

	ca_clear_channel(thread->channel);
	ca_context_destroy();
	ca_pend_io(5.0);

	return NULL;
}

void* EpicsThreadHandler::access_thread_run(void* parm) {

	thread_data* thread = (thread_data*)parm;

	pthread_mutex_lock(&this->context_mutex);
	ca_attach_context(this->CONTEXT);
	pthread_mutex_unlock(&this->context_mutex);

	ca_create_channel(thread->name.c_str(),NULL,NULL,10,&thread->channel);
	ca_pend_io(thread->accessTimeout);
	DEBUG_STREAM << "Epics channel created for PV: " << thread->name << std::endl;

	while(true){

		//1.PART
		pthread_mutex_lock(&(thread->run_mutex));

		while (!thread->run_condition_met) {
			pthread_cond_wait(&thread->run_condition, &thread->run_mutex);
		}
		thread->run_condition_met = 0;
		if(!thread->keepAlive){
			break;
		}
		//1.PART


		//DO STUFF PART
		if(thread->instructions == READ_PV){
			doRead(thread);
		}else if(thread->instructions == WRITE_PV){
			doWrite(thread);
		}
		//DO STUFF PART


		//3.PART
		pthread_mutex_unlock(&thread->run_mutex);
		pthread_mutex_lock(&thread->return_mutex);
		//3.PART


		//5.PART
		thread->return_condition_met = 1;
		pthread_cond_signal(&thread->return_condition);
		pthread_mutex_unlock(&thread->return_mutex);
		//5.PART
	}

	if(thread->state != -1){
		ca_clear_channel(thread->channel);
	}
	ca_detach_context();

	ca_pend_io(5.0);

	return NULL;
}

void* EpicsThreadHandler::context_thread_run(void* parm) {

	//Does stuff, creates context
	context_thread_data* thread = (context_thread_data*)parm;
	pthread_mutex_lock(&this->context_mutex);
	ca_context_create(ca_enable_preemptive_callback);
	this->CONTEXT = ca_current_context();
	pthread_mutex_unlock(&this->context_mutex);
	DEBUG_STREAM << "Epics context created" << std::endl;

	//Signals that the context is created
	pthread_mutex_lock(&thread->done_mutex);
	thread->done_condition_met = 1;
	pthread_cond_signal(&thread->done_condition);
	pthread_mutex_unlock(&thread->done_mutex);


	//Wait for the signal to destroy context and die
	pthread_mutex_lock(&(thread->die_mutex));
	while (!thread->die_condition_met) {
		pthread_cond_wait(&thread->die_condition, &thread->die_mutex);
	}
	ca_context_destroy();
	pthread_mutex_unlock(&(thread->die_mutex));

	return NULL;
}

bool EpicsThreadHandler::registerNew(std::string name, int data_type, double accessTimeout, short size) {
	pthread_mutex_lock(&big_mutex);

	if (threads.find(name) != threads.end()) {
		pthread_mutex_unlock(&big_mutex);
		return false;
	}

	thread_data thread;
	thread.handler = this;
	thread.data_type = data_type;
	thread.name = name;
	thread.size = size;
	thread.accessTimeout = accessTimeout;
	threads[name] = thread;
	pthread_create(&threads[name].thread_id,NULL,access_thread_func,((void*)(&threads[name])));

	pthread_mutex_unlock(&big_mutex);
	return true;
}

bool EpicsThreadHandler::subscribeNew(std::string name, int data_type, double subscriptionCycle, void (*eventCallback)(event_handler_args), void* callbackParam) {

	pthread_mutex_lock(&sub_big_mutex);

	if (sub_threads.find(name) != sub_threads.end()) {
		pthread_mutex_unlock(&sub_big_mutex);
		return false;
	}

	sub_thread_data thread;
	thread.handler = this;
	thread.data_type = data_type;
	thread.name = name;
	thread.subscriptionCycle = subscriptionCycle;
	thread.eventCallback = eventCallback;
	thread.callbackParam = callbackParam;
	sub_threads[name] = thread;
	pthread_create(&sub_threads[name].thread_id,NULL,subscription_thread_func,((void*)(&sub_threads[name])));

	pthread_mutex_unlock(&sub_big_mutex);
	return true;
}

void EpicsThreadHandler::runAndWait(std::string name, int instr, void* value,string& error ,long *size) {

  error.clear();

	pthread_mutex_lock(&big_mutex);
	if (threads.find(name) == threads.end()) {
    error = name + " not registered";
    pthread_mutex_unlock(&big_mutex);
		return;
	}

	thread_data* thread = &(threads[name]);
	if(size) {
		thread->size = *size;
	} else {
    thread->size = 1;
  }

  thread->error.clear();

	//into the queue
	pthread_mutex_lock(&(thread->queue_mutex));

	//2.PART
	pthread_mutex_lock(&(thread->run_mutex));
	pthread_mutex_unlock(&big_mutex);

	thread->instructions = instr;
	thread->value = value;
	thread->run_condition_met = 1;

	pthread_cond_signal(&(thread->run_condition));

	pthread_mutex_lock(&(thread->return_mutex));
	pthread_mutex_unlock(&(thread->run_mutex));
	//2.PART


	//4.PART
	while (!thread->return_condition_met) {
		pthread_cond_wait(&(thread->return_condition), &(thread->return_mutex));
	}
	//4.PART


	//6.PART
	thread->return_condition_met = 0;
	pthread_mutex_unlock(&(thread->return_mutex));
	//6.PART

	//out of the queue
	pthread_mutex_unlock(&(thread->queue_mutex));

  if(size) *size = thread->size;
  error = thread->error;

}

std::string EpicsThreadHandler::getPVUnit(const char* pv_name, double timeout) {

	UnitFuncParam* param = new UnitFuncParam();
	param->pv_name = pv_name;
	param->timeout = timeout;
	pthread_t thread_id;
	pthread_create(&thread_id,NULL,getPVUnit_func,param);
	pthread_join(thread_id, NULL);
	std::string unit = param->units;
	delete param;
	return unit;
}

EpicsThreadHandler::~EpicsThreadHandler() {

	for(std::map<std::string, sub_thread_data>::iterator iterator = sub_threads.begin(); iterator != sub_threads.end(); ++iterator) {

		pthread_mutex_lock(&iterator->second.mutex);

		iterator->second.keepAlive = false;

		pthread_mutex_unlock(&iterator->second.mutex);

	}

	for(std::map<std::string, sub_thread_data>::iterator iterator = sub_threads.begin(); iterator != sub_threads.end(); ++iterator) {

		pthread_join(iterator->second.thread_id, NULL);

	}

	for(std::map<std::string, thread_data>::iterator iterator = threads.begin(); iterator != threads.end(); ++iterator) {
		pthread_mutex_lock(&iterator->second.queue_mutex);

		pthread_mutex_lock(&iterator->second.run_mutex);

		iterator->second.keepAlive = false;
		iterator->second.run_condition_met = 1;
		pthread_cond_signal(&(iterator->second.run_condition));

		pthread_mutex_unlock(&iterator->second.run_mutex);
		pthread_join(iterator->second.thread_id, NULL);

		pthread_mutex_unlock(&iterator->second.queue_mutex);
	}

	//Signals the context thread that it can destroy the context and die
	pthread_mutex_lock(&context_thread.die_mutex);
	context_thread.die_condition_met = 1;
	pthread_cond_signal(&context_thread.die_condition);
	pthread_mutex_unlock(&context_thread.die_mutex);
	pthread_join(context_thread.thread_id, NULL);

}
