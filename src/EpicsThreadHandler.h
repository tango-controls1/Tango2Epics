//=============================================================================
//
// file :        EpicsThreadHandler.h
//
// description : Class for managing epics operations.
//
// project : Tango2Epics
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Author: Vid Juvan $
//
//=============================================================================

#ifndef EPICSTHREADHANDLER_H_
#define EPICSTHREADHANDLER_H_

#include <map>
#include <pthread.h>
#include <stdio.h>
#include <tango.h>
#include <cadef.h>

#define READ_PV 0
#define WRITE_PV 1

class EpicsThreadHandler: public Tango::LogAdapter {

private:

	struct UnitFuncParam{
		double timeout;
		const char* pv_name;
		std::string units = "";
	};

	struct sub_thread_data{
		EpicsThreadHandler* handler;						//Pointer to the instance of this class. Used in static methods.
		std::string name;									//Name of the PV.
		int data_type;										//Epics data type of the PV.
		bool keepAlive = true;								//Boolean for stating if the pthread should stay alive or die.
		int rc;												//
		chid channel;										//Channel id.
		evid event;											//Subscription event id.
		double subscriptionCycle;							//Subscription cycle timeout.
		void (*eventCallback)(event_handler_args);			//Subscription event callback function.
		void* callbackParam;								//Pointer to the parameters for the callback function.
		pthread_t thread_id;								//Thread id.
		pthread_mutex_t mutex = PTHREAD_MUTEX_INITIALIZER;	//Mutex for safe access to the above data.
	};

	struct thread_data{
		EpicsThreadHandler* handler;			//Pointer to the instance of this class. Used in static methods.
		bool keepAlive = true;					//Boolean for stating if the pthread should stay alive or die.
		int instructions;						//0 -> Read, 1 -> Write.
		int data_type;							//Epics data type of the PV.
		void* value;							//Pointer to the value for reading or writing.
		unsigned long size;						//Size. If scalar: 1. If array, then this is the size of the array.
		pthread_t thread_id;					//Thread id.
		std::string name;						//Name of the PV.
		int state = 0;							//CA state of the PV.
		chid channel;							//Channel id.
		double accessTimeout;					//Timeout for accessing the PV.
		bool prevConnected;						//Boolean for indicating if PV previously connected
    std::string error;            // Error string (if any)

		/**
		 * Condition and mutex.
		 * The pthread is waiting on the condition.
		 * Triggered when the pthread is required to run.
		 */
		pthread_cond_t      run_condition  = PTHREAD_COND_INITIALIZER;
		bool run_condition_met = false;
		pthread_mutex_t     run_mutex = PTHREAD_MUTEX_INITIALIZER;

		/**
		 * Condition and mutex.
		 * When the pthread is running, the requesting thread waits on this condition.
		 * Triggered when the pthread is done.
		 */
		pthread_cond_t      return_condition  = PTHREAD_COND_INITIALIZER;
		bool return_condition_met = false;
		pthread_mutex_t     return_mutex = PTHREAD_MUTEX_INITIALIZER;

		/**
		 * Mutex used for serializing requests for one PV.
		 */
		pthread_mutex_t     queue_mutex = PTHREAD_MUTEX_INITIALIZER;

	};


	struct context_thread_data{
		EpicsThreadHandler* handler;			//Pointer to the instance of this class. Used in static methods.
		pthread_t thread_id;					//Thread id.

		/**
		 * Condition and mutex.
		 * Calling thread waits for the condition before continuing.
		 * Triggered when the context is created.
		 */
		pthread_cond_t      done_condition  = PTHREAD_COND_INITIALIZER;
		bool done_condition_met = false;
		pthread_mutex_t     done_mutex = PTHREAD_MUTEX_INITIALIZER;

		/**
		 * Condition and mutex.
		 * Pthread waits for the condition to destroy the context and die.
		 * Triggered when destroying EpicsThreadHandler.
		 */
		pthread_cond_t      die_condition  = PTHREAD_COND_INITIALIZER;
		bool die_condition_met = false;
		pthread_mutex_t     die_mutex = PTHREAD_MUTEX_INITIALIZER;
	};

	ca_client_context *CONTEXT = NULL;
	pthread_mutex_t context_mutex = PTHREAD_MUTEX_INITIALIZER;
	context_thread_data context_thread;


	/**
	 * Map for storing epics access threads.
	 */
	std::map<std::string, thread_data> threads;
	/**
	 * Mutex for epics access thread protection.
	 */
	pthread_mutex_t     big_mutex = PTHREAD_MUTEX_INITIALIZER;

	/**
	 * Map for storing epics subscription threads.
	 */
	std::map<std::string, sub_thread_data> sub_threads;
	/**
	 * Mutex for epics subscription thread protection.
	 */
	pthread_mutex_t     sub_big_mutex = PTHREAD_MUTEX_INITIALIZER;


  /**
   * Internal function for checking PV state.
   * Executed by epics access threads.
   */
  bool checkConn(thread_data* thread);

	/**
	 * Internal function for reading PV value.
	 * Executed by epics access threads.
	 */
	void doRead(thread_data* thread);

	/**
	 * Internal function for writing PV value.
	 * Executed by epics access threads.
	 */
	void doWrite(thread_data* thread);

	/**
	 * Static function of an epics thread that reads PV unit.
	 */
	static void* getPVUnit_func(void* param){
		UnitFuncParam* parameters = (UnitFuncParam*)param;
		chid chan;
		int status;

		status = ca_create_channel(parameters->pv_name, 0, 0, 0, &chan);
		status = ca_pend_io(parameters->timeout);
		if (status != ECA_NORMAL){
			return NULL;
		}

		struct dbr_ctrl_double * pTD;
		unsigned nBytes = dbr_size_n (DBR_CTRL_DOUBLE, 1);
		pTD = (struct dbr_ctrl_double *) malloc (nBytes);
		if (!pTD) {
			return NULL;
		}
		ca_get(DBR_CTRL_DOUBLE,chan,pTD);
		status = ca_pend_io (parameters->timeout);
		if (status != ECA_NORMAL) {
			return NULL;
		}
		parameters->units = pTD->units;

		ca_clear_channel( chan );
		ca_task_exit();
		free(pTD);
		return NULL;
	}

	/**
	 * Static function of an epics access thread.
	 */
	static void* access_thread_func(void *parm){
		return ((thread_data*)parm)->handler->access_thread_run(parm);
	}

	/**
	 * Static function of an epics subscription thread.
	 */
	static void* subscription_thread_func(void *parm){
		return ((thread_data*)parm)->handler->subscription_thread_run(parm);
	}

	/**
	 * Static function of a context thread.
	 */
	static void* context_thread_func(void *parm){
		return ((context_thread_data*)parm)->handler->context_thread_run(parm);
	}

	/**
	 * Function for performing actual subscription.
	 * Executed by epics subscription threads.
	 * Epics subscription thread runs within this function, while kept alive.
	 */
	void* subscription_thread_run(void* parm);

	/**
	 * Function for performing actual PV access.
	 * Executed by epics access threads.
	 * Epics access thread waits for a trigger, and performs read/write on command.
	 * Thread loops as long as kept alive.
	 */
	void* access_thread_run(void* parm);

	/**
	 * Function for creating EPICS context.
	 * Executed at initialization.
	 * Thread creates EPICS context. Other EPICS threads attach to this context.
	 * At destruction, after all EPICS threads have detached, this thread receives a signal to destroy the context and die.
	 */
	void* context_thread_run(void* parm);

public:
	/**
	 * Constructor
	 */
	EpicsThreadHandler(Tango::DeviceImpl *device): Tango::LogAdapter(device){

		context_thread.handler = this;

		//creates context thread and waits for it to create context before continuing
		pthread_mutex_lock(&(context_thread.done_mutex));
		pthread_create(&(context_thread.thread_id),NULL,context_thread_func,((void*)(&context_thread)));
		while (!context_thread.done_condition_met) {
			pthread_cond_wait(&context_thread.done_condition, &context_thread.done_mutex);
		}
		pthread_mutex_unlock(&(context_thread.done_mutex));
	};

	/**
	 * Destructor
	 */
	~EpicsThreadHandler();

	/**
	 * Function for registering a new PV. A PV must be registered before it can be accessed, to establish its thread.
	 * If this returns false, then the name is already mapped. It is not an error! Can normally call runAndWait.
	 * One name cannot have two assigned threads or two channels.
	 * Can register only once per PV.
	 */
	bool registerNew(std::string name, int data_type, double accessTimeout, short size = 1);

	/**
	 * Function for subscribing to a new PV.
	 * Registration for subscription not required.
	 * Can subscribe only once per PV.
	 * If the function returns false, it is an error.
	 */
	bool subscribeNew(std::string name, int data_type, double subscriptionCycle, void (*eventCallback)(event_handler_args), void* callbackParam);

	/**
	 * Function that is to be called when trying to access(read/write) a certain PV.
	 * @instr: 0 -> read, 1 -> write
	 */
	void runAndWait(std::string name, int instr, void* value, std::string& error, long *size = NULL);

	/**
	 * Function to acquire a unit of a certain PV.
	 */
	std::string getPVUnit(const char* pv_name, double timeout);

};



#endif /* EPICSTHREADHANDLER_H_ */
