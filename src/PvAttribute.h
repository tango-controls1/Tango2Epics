//=============================================================================
//
// file :        PvAttribute.h
//
// description : Extended  Tango Attributes, used for epics communications.
//
// project : Tango2Epics
//
// This file is part of Tango device class.
//
// Tango is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Tango is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Tango.  If not, see <http://www.gnu.org/licenses/>.
//
// $Author: Vid Juvan $
//
//=============================================================================

#ifndef PVATTRIBUTE_H_
#define PVATTRIBUTE_H_


#include "cadef.h"

template <class T>
class PvScalarAttribute: public Tango::Attr {

public:

	std::string pv_name = "";
	long data_type;
	int data_type_epics;
	T read_data[1] = {0};
	T write_data[1] = {0};
  bool setpointInit;
	EpicsThreadHandler* threadHandler;
  bool alwaysUpdateSetpoints;

	PvScalarAttribute(const char *_pv_name, const char* att_name,long _data_type, int _data_type_epics, const char* unit, Tango::AttrWriteType w_type, EpicsThreadHandler* _threadHandler, double accessTimeout,  bool alwaysUpdateSetpoints)
		:Attr(att_name,_data_type,w_type) {

		threadHandler = _threadHandler;
		threadHandler->registerNew(_pv_name,_data_type_epics,accessTimeout);

    this->alwaysUpdateSetpoints = alwaysUpdateSetpoints;
    data_type = _data_type;
		data_type_epics = _data_type_epics;
		pv_name = _pv_name;
    setpointInit = false;

		Tango::UserDefaultAttrProp* props = new Tango::UserDefaultAttrProp();
		props->set_unit(unit);
		props->set_display_unit(unit);
		props->set_standard_unit(unit);
		set_default_properties((*props));
	};
	~PvScalarAttribute() {};

	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att){

		//std::cout << "R THREAD HANLDER START" << std::endl;	//DEBUG only
    string error;
		threadHandler->runAndWait(pv_name,READ_PV,(void *)read_data,error);
    if(!error.empty())
      Tango::Except::throw_exception("PVReadError",
                                     error,
                                     "PvScalarAttribute::read()",
                                     Tango::WARN);

	  att.set_value(read_data);

    if( !setpointInit && att.get_writable()==Tango::AttrWriteType::READ_WRITE ) {
      Tango::WAttribute &attw = dev->get_device_attr()->get_w_attr_by_name(att.get_name().c_str());
      attw.set_write_value(read_data[0]);
      if(!alwaysUpdateSetpoints)
        setpointInit = true;
    }
		//std::cout << "R THREAD HANLDER END" << std::endl;		//DEBUG only
	}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att){

		att.get_write_value(write_data[0]);

		//std::cout << "W THREAD HANLDER START" << std::endl;	//DEBUG only
    string error;
		threadHandler->runAndWait(pv_name,WRITE_PV,(void *)write_data,error);
    if(!error.empty())
      Tango::Except::throw_exception("PVWriteError",
                                     error,
                                     "PvScalarAttribute::write()",
                                     Tango::WARN);
		//std::cout << "W THREAD HANLDER END" << std::endl;		//DEBUG only

	}

};


class PvStringAttribute: public Tango::Attr{

public:

	std::string pv_name = "";
	long data_type;
	int data_type_epics;

	char* read_data = new char[256];
	char* write_data;

	EpicsThreadHandler* threadHandler;

	PvStringAttribute(const char *_pv_name, const char* att_name,long _data_type, int _data_type_epics, const char* unit, Tango::AttrWriteType w_type, EpicsThreadHandler* _threadHandler, double accessTimeout)
		:Attr(att_name,_data_type,w_type) {

		threadHandler = _threadHandler;
		threadHandler->registerNew(_pv_name,_data_type_epics,accessTimeout);

		data_type = _data_type;
		data_type_epics = _data_type_epics;
		pv_name = _pv_name;

		Tango::UserDefaultAttrProp* props = new Tango::UserDefaultAttrProp();
		props->set_unit(unit);
		props->set_display_unit(unit);
		props->set_standard_unit(unit);
		set_default_properties((*props));
	};
	~PvStringAttribute() {
		delete[] read_data;
	};

	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att){

		//std::cout << "R THREAD HANLDER START" << std::endl;	//DEBUG only
    string error;
		threadHandler->runAndWait(pv_name,READ_PV,(void *)read_data,error);
    if(!error.empty())
        Tango::Except::throw_exception("PVReadError",
                                       error,
                                       "PvSpectrumAttribute::read()",
                                       Tango::WARN);

  	att.set_value(&read_data);
		//std::cout << "R THREAD HANLDER END" << std::endl;		//DEBUG only

	}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att){

		att.get_write_value(write_data);

		//std::cout << "W THREAD HANLDER START" << std::endl;	//DEBUG only
    string error;
		threadHandler->runAndWait(pv_name,WRITE_PV,(void *)write_data,error);
    if(!error.empty())
      Tango::Except::throw_exception("PVWriteError",
                                     error,
                                     "PvStringAttribute::write()",
                                     Tango::WARN);
		//std::cout << "W THREAD HANLDER END" << std::endl;		//DEBUG only

	}

};


template <class T>
class PvSpectrumAttribute: public Tango::SpectrumAttr{

public:

	std::string pv_name = "";
	long data_type;
	int data_type_epics;
	long max_size;
	T* read_data;
	const T* write_data;
	EpicsThreadHandler* threadHandler;
  bool setpointInit;
  bool alwaysUpdateSetpoints;

	PvSpectrumAttribute(const char *_pv_name, const char* att_name,long _data_type, int _data_type_epics, const char* unit, Tango::AttrWriteType w_type, long _max_size, EpicsThreadHandler* _threadHandler, double accessTimeout, bool alwaysUpdateSetpoints)
		:SpectrumAttr(att_name,_data_type,w_type, _max_size) {

		threadHandler = _threadHandler;
		threadHandler->registerNew(_pv_name,_data_type_epics, accessTimeout, _max_size);

    this->alwaysUpdateSetpoints = alwaysUpdateSetpoints;
		max_size = _max_size;
		read_data = new T[max_size];
		data_type = _data_type;
		data_type_epics = _data_type_epics;
		pv_name = _pv_name;

		Tango::UserDefaultAttrProp* props = new Tango::UserDefaultAttrProp();
		props->set_unit(unit);
		props->set_display_unit(unit);
		props->set_standard_unit(unit);
		set_default_properties((*props));
    setpointInit = false;

	};
	~PvSpectrumAttribute() {
		delete[] read_data;
	};

	virtual void read(Tango::DeviceImpl *dev,Tango::Attribute &att){

		//std::cout << "R THREAD HANLDER START" << std::endl;	//DEBUG only
    long nbRead = max_size;
    string error;
		threadHandler->runAndWait(pv_name,READ_PV,(void *)read_data,error,&nbRead);
    if(!error.empty())
      Tango::Except::throw_exception("PVReadError",
                                     error,
                                     "PvSpectrumAttribute::read()",
                                     Tango::WARN);

  	att.set_value(read_data,nbRead);

    if( !setpointInit && att.get_writable()==Tango::AttrWriteType::READ_WRITE ) {
      Tango::WAttribute &attw = dev->get_device_attr()->get_w_attr_by_name(att.get_name().c_str());
      attw.set_write_value(read_data,nbRead);
      if(!alwaysUpdateSetpoints)
        setpointInit = true;
    }

		//std::cout << "R THREAD HANLDER END" << std::endl;		//DEBUG only

	}
	virtual void write(Tango::DeviceImpl *dev,Tango::WAttribute &att){

		att.get_write_value(write_data);
		long length = att.get_write_value_length();

		//std::cout << "W THREAD HANLDER START" << std::endl;	//DEBUG only
    string error;
		threadHandler->runAndWait(pv_name,WRITE_PV,(void *)write_data,error ,&length);
    if(!error.empty())
      Tango::Except::throw_exception("PVWriteError",
                                     error,
                                     "PvSpectrumAttribute::write()",
                                     Tango::WARN);

		//std::cout << "W THREAD HANLDER END" << std::endl;		//DEBUG only

	}

};



#endif /* PVATTRIBUTE_H_ */
