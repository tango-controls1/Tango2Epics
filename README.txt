Tango2Epics gateway is a Tango device server used for the integration of the devices with EPICS support in Tango control system. 
By using the Tango2Epics gateway, integration can be achieved through software configuration; no development is needed.

Please refer to the Tango2Epics Gateway User's Manual (doc/Tango2EpicsGateway.pdf) for the instructions on how to build and use this device server.

Tango2Epics Gateway package is organized in the following directories:

src/           .... source code
configuration/ .... gateway configuration file and database import script 
demo/          .... EPICS IOC simulator and client test script
doc/           .... User's manual and html documentation

Migrated from https://svn.code.sf.net/p/tango-ds/code/DeviceClasses/Communication/Tango2Epics
