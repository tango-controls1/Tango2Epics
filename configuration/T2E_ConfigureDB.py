#    
#    This script is a part of the Tango2Epics project.
#    The script populates the Tango database, according to the configuration file.
#    Tango Instance Name ought to be provided as the first argument. 
#    Path to the configuration file ought to be provided as the second argument.
#    Progress and results are displayed via console.
#


if __name__ == "__main__":
    import sys
    import PyTango

    if(len(sys.argv) < 3):
        print("Usage: T2E_ConfigureDB.py instance_name conf_file_path")
        sys.exit(1)
        
    server_name = "Tango2Epics/" + sys.argv[1]
    class_name = "Tango2Epics"
    
    errors = []
    try:
        db = PyTango.Database()
        dev_info = PyTango.DbDevInfo()
        dev_info.server = server_name
        dev_info._class = class_name
        
        for index in range (2,len(sys.argv)):
            file_path = sys.argv[index]
        
            with open(file_path, 'r') as content_file:
                content = content_file.read()
            content = content.replace('\n', "")

            
            configurations = content.split("#");
            for configuration in configurations:
                if ";" in configuration:
                    device_properties = configuration.split(";")
                    device_name = device_properties.pop(0).strip()
                    dev_info.name = device_name
                    db.add_device(dev_info)
                    
                    print("\nPopulating Database for Device: " + device_name)
                    
                    properties = {}

                    for device_property in device_properties:
                        if "|" in device_property:
                            dproperty = device_property.split("|")
                            values = dproperty[1].rstrip('\n').strip().split(',')
                            properties[dproperty[0]] = (values)
                            print("Adding Property: " + dproperty[0])
                            for value in values:
                                print("    " + value)
                        elif device_property != "":
                            errors.append(device_name + ": \nIllegal item in the configuration: " + device_property + ". Item was skipped!")
                            #print("Illegal item in the configuration: " + device_property)
                            #print("Item was skipped!")

                    db.put_device_property(device_name, properties)
        
        if len(errors) > 0:
            print("\nErrors were found in the configuration:")
            for error in errors:
                print(error)
        else:
            print("\nDatabase was successfully populated!")
            
    except:
        print ("Illegal Syntax in Configuration file")
        sys.exit(1)
